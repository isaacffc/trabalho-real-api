const axios = require('axios');


axios.get("http://senacao.tk/objetos/veiculo_array_objeto")
.then(function(res) {
    const user = res.data;
    
    console.log(`Marca: ${user.marca}`);
    console.log(`Modelo: ${user.modelo}`);
    console.log(`Ano: ${user.ano}`);
    console.log(`Quilometragem: ${user.quilometragem}`);
    // console.log(`Opcional 1: ${user.opcionais[0]}`);
    // console.log(`Opcional 2: ${user.opcionais[1]}`);
    // console.log(`Opcional 3: ${user.opcionais[2]}`);


    user.opcionais.forEach(function(o) {
        console.log(`Opcionais: ${o}`);

    });

    console.log("Vendedor:")
    console.log(`Nome: ${user.vendedor.nome}`);
    console.log(`Idade: ${user.vendedor.idade}`);
    console.log(`Celular: ${user.vendedor.celular}`);
    console.log(`Cidade: ${user.vendedor.cidade}`);


});