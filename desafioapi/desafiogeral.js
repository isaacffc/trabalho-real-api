const axios = require('axios')
axios.get("http://senacao.tk/objetos/usuario")
.then(function(res) {
    const user = res.data;

    console.log(`Nome: ${user.nome}`);
    console.log(`Email: ${user.email}`);
    console.log(`Telefone: ${user.telefone}`);
    console.log('----');

    user.conhecimentos.forEach(function(c, index) {
        console.log(`Conhecimentos: ${index} ${c}`)
        console.log('----');
    
    });
    
    console.log("Endereço:")
    console.log(`Rua: ${user.endereco.rua}`);
    console.log(`Nº: ${user.endereco.numero}`);
    console.log(`Bairro: ${user.endereco.bairro}`);
    console.log(`Cidade: ${user.endereco.cidade}`);
    console.log(`UF: ${user.endereco.uf}`);
    console.log('----');
    

    user.qualificacoes.forEach(function(q) {
        console.log(`Nome: ${q.nome}`);
        console.log(`Instituição: ${q.instituicao}`);
        console.log(`Ano: ${q.ano}`);
        console.log('----');


    })
    
   
    
});